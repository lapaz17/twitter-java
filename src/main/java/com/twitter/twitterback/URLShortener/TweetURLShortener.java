package com.twitter.twitterback.URLShortener;

//functional requirements
//Generate shorter and unique aliases of the longer URL's
//When the users click the short link they will be redirected to the original link
//        Users should optionally be able to pick a custom short link for their URL
//        Links will expire after a standard default timespan. Users should be able to specify the expiration time.



//Nonfunctional requirements
//        The system should be highly available. This is required because, if our service is down, all the URL redirections will start failing.
//        URL redirection should happen in real-time with minimal latency.
//        Shortened links should not be guessable (not predictable).


public class TweetURLShortener {

}
